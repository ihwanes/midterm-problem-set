public class Spear extends Weapon {

    public Spear(String name, int attackPoint, int defensePoint, int durabilityPoint) {
        super(name, attackPoint, defensePoint, durabilityPoint);
    }

    @Override
    public void chantSpell(Character aCharacter){
        System.out.println("Unfortunately, "+this.getName()+" cannot be used to cast an offensive spell");
    }

    @Override
    public void chantHealingPoem(Character aCharacter){
        System.out.println("Unfortunately, "+this.getName()+" cannot be used to cast a healing spell");
    }

}
