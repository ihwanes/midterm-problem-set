public class Assasin extends Character {

    public Assasin(String name) {
        this.name = name;
        this.level = 1;
        this.baseHitpoints = 600;
        this.baseManapoints = 900;
        this.baseAgility = 20;
        this.baseStrength = 5;
        this.baseIntelligence = 5;
    }

    public String dropLeftWeapon(){
        this.rightHandWeapon = null;
        return this.rightHandWeapon.getClass().getSimpleName()+" is dropped from your right hand";
    }

    public String setLeftWeapon(Weapon weapon){
        String retMsg = "Successfully equip "+weapon.getName();
        if(this.leftHandWeapon == null){
            if(weapon instanceof Knife){
                this.leftHandWeapon = weapon;
            } else if (weapon instanceof Broadsword && (this.rightHandWeapon == null || !(this.rightHandWeapon instanceof Broadsword))){
                this.leftHandWeapon = weapon;
            }
            else{
                retMsg = "Sorry, "+this.getClass().getSimpleName()+" cannot wield "+weapon.getClass().getSimpleName()+" as a weapon";
            }
        }else{
            retMsg = "Hands already full. Drop your current weapon first";
        }
        return retMsg;
    }

    public String dropRightWeapon(){
        this.rightHandWeapon = null;
        return this.rightHandWeapon.getClass().getSimpleName()+" is dropped from your right hand";
    }

    public String setRightWeapon(Weapon weapon){
        String retMsg = "Successfully equip "+weapon.getName();
        if(this.rightHandWeapon == null){
            if(weapon instanceof Knife){
                this.rightHandWeapon = weapon;
            } else if (weapon instanceof Broadsword && (this.leftHandWeapon == null || !(this.leftHandWeapon instanceof Broadsword))){
                this.rightHandWeapon = weapon;
            }else{
                retMsg = "Sorry, "+this.getName()+" cannot wield "+weapon.getClass().getSimpleName()+" as a weapon";
            }
        }else{
            retMsg = "Hands already full. Drop your current weapon first";
        }
        return retMsg;
    }
}
