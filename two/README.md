# Problem Set 2
## Behavioral Design Pattern

You are asked to improve the design of a simple `FortuneWheel` game.
Below are brief explanations for each behavior that can be performed in FortuneWheel simulation:
1. rollTheWheel(int guessedNumber): Player will call this method to start the game by spinning the wheel while guessing which number will be shown after the wheel stopped spinning. The player will choose between 0-9.
2. instantStop(): Player will call this method to stop the wheel immediately.
3. stopSlowly(): Player will call this method to stop the wheel after the count of three.
4. rollingResult(): Player can check which number pointed by the wheel after it stopped completely.

The owner of the `FortuneWheel` worries with the current design.
To make the game more interesting, he decided to change the behavior of the `FortuneWheel`. 
Unfortunately, the design of `FortuneWheel` class is cluttered with branching statements. 
The owner worries that if he try to add new behavior using current design, it will create an unknown side effect. 
**You are asked to refactor the FortuneWheel class using your knowledge of Design Principles and Patterns**.

### How to use

```java
javac FortuneWheelSimulation.java
java FortuneWheelSimulation
```

### Questions

1. Which Design Principles violated the above code? Explain your reasoning for each violated Design Principles you mentioned!
```
Single Responsibility Principle: Karena beberapa state yang ada antar method yang berulang-ulang, seperti bagaimana dia Rolling dsb.
```
2. Choose one of the Behavioral Design Patterns that is most suitable to improve the above code? Explain the reason why that pattern is most suitable using 1 - 3 sentences!
```
State pattern, karena setiap method yang ada mengandung beberapa state, dan perlu di-refactor menjadi beberapa state behaviours yang berbeda.
```
3. Apply refactoring steps to the above code snippet based on your chosen pattern from Question number 2! 
**Make sure that each refactoring step is illustrated using one git commit.**