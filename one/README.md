# Problem Set 1

## Structural & Creational Design Pattern

You are asked to make a simple `ElectronicDevices` simulation. 
The simulation that will be done is to turn on and turn off `SmartTV`.
A `SmartTV` has several forming components such as `LCDScreen`, `Motherboard`, and `PowerSupply`.
If someone turns on `SmartTV`, then all the components that compose it (`LCDScreen`, `Motherboard`, and `PowerSupply`) must be turned on. 
Turning off `SmartTV` means turning off all the components that make it up. 
**You are asked to make sure the `ElectronicDevicesSimulator` class is unaffected if there are changes in the logic of switching the `SmartTv` on and off**. 
Any other problems are irrelevant in this case. Below are several questions that need to be answered

### How to use

```java
javac ElectronicDevicesSimulator.java
java ElectronicDevicesSimulator
```

### Questions

1. Should we apply the Structural Design Patterns to correct the code snippet above? Explain your reason based on related Design Principles!
```
Iya, karena kita tidak perlu mengetahui kompleksitas dari keseluruhan program, cukup 1 program yang mewakili semuanya. Hal ini menyangkut Single Responsibility dan Open-Closed Principle.
```
2. Should we apply the Creational Design Patterns to correct the code snippet above? Explain your reason based on related Design Principles!
```
Tidak, karena kita harus memastikan ElectronicDevicesSimulator tidak terpengaruh perubahan yang dilakukan oleh class-class yang lain
```
3. Apply refactoring steps to the above code snippet based on your analysis on Question number 1 and 2! **Make sure that each refactoring step 
is illustrated using one git commit**.
 